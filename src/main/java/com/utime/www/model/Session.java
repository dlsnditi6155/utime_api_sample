package com.utime.www.model;

import java.util.Calendar;

import lombok.Data;

@Data
public class Session {
	private String id;
	private String name;
	private Object state;
	private String viewerUrl;
	private String folderId;
	private String description;
	private Calendar startTime;
	private String thumbUrl;
}
