package com.utime.www.model;

import lombok.Data;

@Data
public class RemoteRecorder {
	private String name;
	private String id;
}
