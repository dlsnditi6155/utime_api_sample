package com.utime.www.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Data;

@Configuration
@ConfigurationProperties(prefix = "api.url")
@Data
public class ApiUrl {
	private String scheduleList;
	private String recordingCreate;
	private String launcherUrl;
	private String recordingList;
	private String folderList;
	private String folderListById;
	private String recordingRecurring;
	private String scheduleEditDesc;
	private String scheduleView;
	private String scheduleEditIsBroadcast;
	private String scheduleDelete;
	private String scheduleViewByIds;
	private String scheduleCreate;
}
