# 샘플프로젝트 구동방법

## Download JDK
java version : OpenJDK 1.8.0 이상
download : https://github.com/ojdkbuild/contrib_jdk8u-ci/releases/download/jdk8u222-b10/jdk-8u222-ojdkbuild-linux-x64.zip

## yml 설정파일
프로젝트 setting_file 폴더에서 가져올 수 있다

## FILE SETTING - 리눅스에서 구동 시 
/home/utime 에 jdk-8u222-ojdkbuild-linux-x64.zip 복사 후

unzip jdk-8u222-ojdkbuild-linux-x64.zip

프로젝트빌드 후 생성 된 파일 UTIME_PANOPTO_VIEWER_SAMPLE.jar 을 /home/utime에 복사

프로젝트 setting_file 폴더에 있는 application.yml 파일 /home/utime/sample_config 에 복사

프로젝트 setting_file 폴더에 있는 start.sh, stop.sh 파일 /home/utime 에 복사

## FILE SETTING - 이클립스로 구동 시
이클립스 설치 드라이버의 /home/utime/sample_config 에 yml 파일 복사

실행 시 Spring Boot App 설정

Run Configuration > Arguments > VM arguments
```
-Dconf.home=/home/utime/sample_config
```

## yml 셋팅

```
panopto.auth:
  authKey: "파납토 LMS 관리자 계정"
  authPwd: "파납토 LMS 관리자 비밀번호"
  endpoint: "UTIME_API 설치 서버 도메인 정보 예<아이피:포트>) 000.000.000.000:8080"
```

## 샘플 기능
1. 즉시세션만들기
2. 예약세션만들기
3. 파납토 어플리케이션 실행하기
4. 목록가져오기 ( 상제정보, 삭제, view주소 )

## Getting Start
SPRING BOOT 설정방법

### RestTemplate configuration
* WebConfiguration.java
```
	@Bean
	public RestTemplate restTemplate() {
	    TrustStrategy acceptingTrustStrategy = new TrustStrategy() {
	        @Override
	        public boolean isTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {
	            return true;
	        }
	    };
	    SSLContext sslContext = null;
		try {
			sslContext = org.apache.http.ssl.SSLContexts.custom().loadTrustMaterial(null, acceptingTrustStrategy).build();
		} catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException e) {
			logger.info(e.getLocalizedMessage());
			logger.info(e.getMessage());
		}
	    SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext, new NoopHostnameVerifier());
	    CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(csf).build();
	    HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
	    requestFactory.setHttpClient(httpClient);
	    RestTemplate restTemplate = new RestTemplate(requestFactory);
	    return restTemplate;
	}
```

* using Controller > PanoptoServiceController.java
```
	@Autowired
	private RestTemplate restTemplate;
	
	@RequestMapping(value = "/pt/schedule_list", method= {RequestMethod.GET, RequestMethod.POST})
    public String schedulelist(
    .
    .
    .
    ) {
    	MultiValueMap<String, Object> params = new LinkedMultiValueMap<String, Object>(); // application/x-www-form-urlencoded 폼 전송
    	/*
    	API에서 사용하는 파라미터 및 포맷은 API 스웨거 참고
    	http://{API IP}:{API PORT}/swagger-ui.html
    	*/
    	if(!startDate.equals("")) params.add("startDate", startDate.replaceAll("-", "")+"000000");
		if(!endDate.equals("")) params.add("endDate", endDate.replaceAll("-", "")+"235959");
		params.add("currentPage", currentPage);
		params.add("page_size", pageSize);
		params.add("folderOwnerId", folderOwnerId);
		params.add("sessionName", sessionName);
		params.add("searchQuery", sessionName); // 제목 검색 시 searchQuery에도 같은 검색어를 넣어주면 검색 시간이 단축된다.
		params.add("sessionState", sessionState);
		params.add("sort", "Name");
		params.add("sortIncreasing", "Asc");
		params.add("userkey", panoptoAuth.getAuthKey());
		params.add("userpwd", panoptoAuth.getAuthPwd());
		
		ResponseEntity<String> ret = restTemplate.postForEntity(panoptoAuth.getApiUrl(panoptoAuth.getEndpoint(), apiUrl.getScheduleList()), params, String.class);
		System.err.println(ret.getBody().toString()); // API 에서 받은 데이트 출력
    }
```

### json 파싱 변수 선언
* Result.java
* ResultArray.java
```
@Data // lombok 사용
public class Result {
	private boolean boolResult;
	private String msg;
	private Object obj;
	private Session session; 
	/*
	세션 목록을 받아오기 위한 변수 선언
	변수명은 스웨거에 Responses > example Value 참고
	EX>
	{
	  "boolResult": true,
	  "msg": "string",
	  "obj": {},
	  "sessions": {
	      "creatorId": "string",
	      "description": "string",
	      .
	      .
	      .
	*/
}

public class ResultArray {
	private boolean boolResult;
	private String msg;
	private Object obj;
	private int total;
	private List<Session> sessions; 
	/*
	세션 목록을 받아오기 위한 변수 선언
	변수명은 스웨거에 Responses > example Value 참고
	EX>
	{
	  "boolResult": true,
	  "msg": "string",
	  "obj": {},
	  "sessions": [ // sessions 배열로 json 리턴을 해주기 떄문에 변수명은 sessions 로 선언한다.
	    {
	      "creatorId": "string",
	      "description": "string",
	      .
	      .
	      .
	*/
}
```

* Session.java
Session 변수명은 스웨거에 Responses > example Value 참고
필요한것만 파싱할 수 있다.
```
@Data
public class Session {
	private String id;
	private String name;
	private Object state;
	private String viewerUrl;
	private String folderId;
	private String description;
	private Calendar startTime;
	private String thumbUrl;
}
```

* Result.java 구조 설명

boolResult : API 호출 성공여부

msg : API 호출 실패 시 실패사유가 입력되어있다.

```
public class Result {
	private boolean boolResult;
	private String msg;
	.
	.
	.
}
```

### session 목록 가져오기 및 활용
* PanoptoServiceController.java
```
// ret에 API가 반환해준 json 형태의 데이타를 파싱하여 가지고 있다. 
ResponseEntity<ResultArray> ret = restTemplate.postForEntity(panoptoAuth.getApiUrl(panoptoAuth.getEndpoint(), apiUrl.getScheduleList()), params, ResultArray.class);
List<Session> list = new ArrayList<Session>();
list = ret.getBody().getSessions();
```

## 기타 언어 
각 언어별 REST API 전용 라이브러리 사용 
